import sys
sys.path.insert(0,'/Users/b1tchacked1/Documents/Fall-2015/Aritificial-Intelligence/Assignments/hw4/aima-python')
import logic
from logic import to_cnf
import itertools

def parse_file(filepath):
    # read the layout file to the board array
    board = []
    fin = open(filepath)
    #Reading the First line of file
    N,M = fin.readline().split()
    N = int(N)
    M = int(M)

    #Initializing the Board as per dimensions
    board=[[0 for col in range(M)] for row in range(N)] 
    
    #Index of Variables in the Output CNF
    index_var=-1

    #Assigning the board as per input
    for row in range(N):
        #Reading Line By Line
        line=fin.readline()
        #Stripping New line Character at End of String
        if line[len(line)-1] == '\n':
            line = line[:-1]

        line=line.split(',')
        for col in range(M):
            if line[col] == 'X': 
                board[row][col] = index_var
                index_var = index_var - 1
            else:
                board[row][col] = int(line[col])

    fin.close()
    return board

#Checks if indices are allowed
def is_row_col_valid(row,col,N,M):
    if row >=0 and row <N and col >=0 and col <M:
        return True
    else:
        return False

def list_of_x(row,col,board):
    #All Possible Directions to go from this row,col
    directions=[(0,-1),(-1,-1),(-1,0),(-1,1),(0,1),(1,1),(1,0),(1,-1)]
    list_of_var = []
    for direction in directions:
        new_row = row + direction[0]
        new_col = col + direction[1]
        #If it has a X increment by 1  
        if is_row_col_valid(new_row,new_col,len(board),len(board[0])) and board[new_row][new_col] < 0:
            list_of_var.append(-board[new_row][new_col])
    return list_of_var

def print_anded_of_variables(list_of_variables):
    result = ""
    result += "( "
    index = 0
    for var in list_of_variables:
        if index == len(list_of_variables)-1:
            result +=  str(var)+" ) "
        else:
            result +=  str(var) + " & "
        index += 1
    return result

def convert_to_logical_expression_for_cell(list_of_combinations):
    result = ""
    result += "( "
    index = 0
    for comb in list_of_combinations:
        if index == len(list_of_combinations)-1:
            result += print_anded_of_variables(comb) + " ) "
        else:
            result += print_anded_of_variables(comb) + " | "
        index += 1
    return result

def convert_to_logical_expression(list_of_cells):
#   return 0
    result = ""
    result += "( "
    index = 0
    for cell in list_of_cells:
        if ( isinstance(cell,int)  and cell == -1):
            result += ' ~ '
            index += 1
            continue
        if index == len(list_of_cells)-1:
            result += convert_to_logical_expression_for_cell(cell) + " )"
        else:
            result += convert_to_logical_expression_for_cell(cell) + " & "
        index += 1
        #print result
    return result

def convertCNF2MiniSatInput(input):
    input=str(input)
    f = open('minisatinput.txt','w')
    no_of_clauses = input.count('&')+1
    no_of_variables = 0
    list_of_numbers=['1','2','3','4','5','6','7','8']
    list_of_found_variables = []

    #Iterating over every character to find distinct variables
    for x in input:
        if x in list_of_numbers:
            if x not in list_of_found_variables:
                list_of_found_variables.append(x)
                no_of_variables += 1
    f.write('p cnf '+ str(no_of_variables)+' '+str(no_of_clauses)+'\n')

    for x in input:
        if x == '~':
            f.write('-')
        if x == '&':
            f.write('0\n')
        if x in list_of_numbers:
            f.write(x+' ')

    f.write('0\n')
    f.close()


def convert2CNF(board, output):
    # interpret the number constraints
    fout = open(output, 'w')
    list_of_combinations = []
    for row in range(len(board)):
        for col in range(len(board[row])):
            no = board[row][col]
            if no < 0 :
                continue
            #Calculate No of X's Surrounded by the current cell and get a list of those variables
            no_x = no
            if no == 0:
                no_x = 1
            list_of_var = list_of_x(row,col,board)
            if len(list_of_var) > 0:
                temp_list_comb = []
                for combination in itertools.combinations(list_of_var,no_x):
                    temp_list_comb.append(list(combination))
                if no == 0 and len(temp_list_comb) > 0:
                    list_of_combinations.append(-1)
                if len(temp_list_comb) > 0:
                    list_of_combinations.append(temp_list_comb)

    #print list_of_combinations
    print(convert_to_logical_expression( list_of_combinations))
    print(to_cnf(convert_to_logical_expression( list_of_combinations)))
    convertCNF2MiniSatInput(to_cnf(convert_to_logical_expression( list_of_combinations)))
    fout.close()


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print 'Layout or output file not specified.'
        exit(-1)
    board = parse_file(sys.argv[1])
    convert2CNF(board, sys.argv[2])
